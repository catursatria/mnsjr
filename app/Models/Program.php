<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
    //
    protected $fillable = [
        'nama',
        'jenis',
        'sejak',
        'hingga',
        'hingga_detail',
        'keterangan',
        'foto',
        'flag',
    ];
    public $timestamps = false;

}
