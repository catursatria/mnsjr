<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Member extends Model
{
    //
    protected $fillable = [
        'nama',
        'no_hp',
        'email',
        'status',
        'status_sekolah',
        'status_kelas3',
        'flag',
        'tempat_lahir',
        'tanggal_lahir',
        'id_pondok_asal',
        'id_tempat_tugas',
        'daerah_sambung',
        'desa_sambung',
        'kelompok_sambung',
        'alamat',
    ];

    public function asalPondok() {
        return $this->belongsTo(DaerahPondok::class, 'id_pondok_asal');
    }

    public function tempatTugas() {
        return $this->belongsTo(DaerahPondok::class, 'id_tempat_tugas');
    }

    public function role() {
        return $this->belongsTo(Status::class, 'status');
    }

}
