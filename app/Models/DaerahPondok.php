<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DaerahPondok extends Model
{
    //
    protected $table = 'daerah_pondok';
    protected $fillable = [
        'nama',
        'jenis',
        'bawah_bimbingan',
        'bawah_bimbingan_lainnya',
        'ruang_lingkup',
        'ruang_lingkup_lainnya',
        'flag',
    ];
    
    public $timestamps = false;

    public function type()
    {
        return $this->belongsTo(Type::class, 'jenis');
    }
}
