<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Visit extends Model
{
    protected $fillable = [
        'id_visitor1',
        'id_visitor2',
        'id_visitor3',
        'id_materi1',
        'id_materi2',
        'id_materi3',
        'id_materi4',
        'id_materi5',
        'id_daerah_pondok',
        'dari',
        'sampai',
        'kendala',
        'kebutuhan',
        'saran',
        'flag'
    ];

    public function visitor1() {
        return $this->belongsTo(Member::class, 'id_visitor1');
    }

    public function visitor2() {
        return $this->belongsTo(Member::class, 'id_visitor2');
    }

    public function visitor3() {
        return $this->belongsTo(Member::class, 'id_visitor3');
    }

    public function daerah() {
        return $this->belongsTo(DaerahPondok::class, 'id_daerah_pondok');
    }

}
