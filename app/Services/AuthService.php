<?php

namespace App\Services;

use App\Services\BaseService;
use Illuminate\Support\Facades\Auth;

class AuthService extends BaseService
{

    public function __construct()
    {
        parent::__construct();
    }

    public function login($request)
    {
        if (Auth::attempt(['email' => $request->email, 'password' => $request->password])) {
            $user = Auth::user();
            $data['access_token'] = $user->createToken('nApp')->accessToken;
            $data['user'] = $user;
            $permissions = array();
            $dataPermissions = $user->getPermissionsViaRoles();

            foreach ($dataPermissions as $item) {
                array_push($permissions, $item->name);
            }

            $data['permissions'] = $permissions;
            return $this->responseMessage('Login Success', 200, true, $data);
        } else {
            return $this->responseMessage('Unauthorised', 401, false);
        }
    }
}
