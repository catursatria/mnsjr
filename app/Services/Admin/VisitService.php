<?php

namespace App\Services\Admin;

use App\Models\Visit;
use App\Models\Pemrek\ReportAgent;
use App\Repositories\Admin\VisitRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class VisitService extends BaseService
{
    protected $repo;

    public function __construct(
        VisitRepository $repo
    ) {
        parent::__construct();
        $this->repo = $repo;
    }

    public function data($request)
    {
        $query = $this->repo->with($request);

        return DataTables::of($query)->addIndexColumn()->make(true);
    }

    public function store($request)
    {
        # code...
        $data = $request->all();
        $visitors = $request->id_visitor1;
        $name1 = 'id_visitor';
        $this->loop($visitors, $name1);
        $name2 = 'id_materi';
        $matters = $request->id_materi1;
        for ($i=1; $i <= count($visitors); $i++) {
            # code...
            $column = 'id_visitor'.$i;
            $a = $i-1;
            $data[$column] = $visitors[$a];
        }
        for ($i=1; $i <= count($matters); $i++) {
            # code...
            $column = 'id_materi'.$i;
            $a = $i-1;
            $data[$column] = $matters[$a];
        }
        $data['dari'] = Carbon::parse($request->dari)->format('Y-m-d');
        $data['sampai'] = Carbon::parse($request->sampai)->format('Y-m-d');
        $this->repo->create($data);
        return redirect()->route('visit.index')->with('success', 'Data has been created.');
    }

    public function update($request, $id)
    {
        # code...
        $data = $request->all();
        $visitors = $request->id_visitor1;
        $matters = $request->id_materi1;
        for ($i=1; $i <= count($visitors); $i++) {
            # code...
            $column = 'id_visitor'.$i;
            $a = $i-1;
            $data[$column] = $visitors[$a];
        }
        for ($i=1; $i <= count($matters); $i++) {
            # code...
            $column = 'id_materi'.$i;
            $a = $i-1;
            $data[$column] = $matters[$a];
        }
        $this->repo->getById($id);
        $data['tanggal'] = Carbon::parse($request->tanggal)->format('Y-m-d');
        $this->repo->update($data, $id);

        return redirect()->route('visit.index')->with('success', 'Data has been updated.');
    }

    public function destroy($id)
    {
        // $item = Visit::find($id);
        // $data['flag'] = 2;
        $this->repo->getById($id)->delete();
        // $item->update($data, $id);
    }
}
