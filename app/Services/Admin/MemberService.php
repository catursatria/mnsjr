<?php

namespace App\Services\Admin;

use App\Models\Member;
use App\Models\Pemrek\ReportAgent;
use App\Repositories\Admin\MemberRepository;
use App\Services\BaseService;
use Carbon\Carbon;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class MemberService extends BaseService
{
    protected $repo;

    public function __construct(
        MemberRepository $repo
    ) {
        parent::__construct();
        $this->repo = $repo;
    }

    public function data($request)
    {
        $query = $this->repo->with($request);

        return DataTables::of($query)->addIndexColumn()->make(true);
    }

    public function store($request)
    {
        $data = $request->all();
        $data['status_kelas3'] = $data['status_kelas3'] == null ? 0 : $data['status_kelas3'];
        $data['tanggal_lahir'] = Carbon::parse($request->tanggal_lahir)->format('Y-m-d');
        $this->repo->create($data);
        return $data;
    }

    public function update($request, $id)
    {
        $data = $request->all();
        $this->repo->getById($id);
        $data['status_kelas3'] = $data['status_kelas3'] == null ? 0 : $data['status_kelas3'];
        $data['tanggal_lahir'] = Carbon::parse($request->tanggal_lahir)->format('Y-m-d');
        $this->repo->update($data, $id);

        return $data;
    }

    public function destroy($id)
    {
        $this->repo->getById($id)->delete();
    }
}
