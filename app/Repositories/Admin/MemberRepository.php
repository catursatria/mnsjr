<?php

namespace App\Repositories\Admin;

use App\Models\Member;
use App\Models\User\User;
use App\Repositories\BaseRepository;

class MemberRepository extends BaseRepository
{
    public function __construct(Member $model)
    {
        $this->model = $model;
    }


    public function with($request)
    {
        $datas = $this->model->with('asalPondok', 'tempatTugas', 'role');

        // if (isset($request->klien)) {
        //     # code...
        //     $datas->where('user_id', $request->klien );
        // }

        $datas = $datas->get();
        return $datas;
    }
}
