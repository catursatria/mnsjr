<?php

namespace App\Repositories\Admin;

use App\Models\Visit;
use App\Repositories\BaseRepository;

class VisitRepository extends BaseRepository
{
    public function __construct(Visit $model)
    {
        $this->model = $model;
    }


    public function with($request)
    {
        $datas = $this->model->with('visitor1', 'visitor2', 'visitor3', 'daerah');

        // if (isset($request->klien)) {
        //     # code...
        //     $datas->where('user_id', $request->klien );
        // }

        $datas = $datas->get();
        return $datas;
    }
}
