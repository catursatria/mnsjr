<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DaerahPondok;
use App\Models\Matter;
use App\Models\Member;
use App\Models\Role;
use App\Models\Visit;
use App\Services\Admin\VisitService;
use Illuminate\Http\Request;

class VisitController extends Controller
{
    protected $service;

    public function __construct(
        VisitService $service
    )
    {
        $this->service = $service;
    }

    public function index()
    {
        # code...
        return view('pages.admin.visit.index');
    }

    public function data(Request $request)
    {
        # code...
        return $this->service->data($request);
    }

    public function create()
    {
        $visitor = Member::where('id_role', '4')->get();
        $daerahPondok = DaerahPondok::where('flag', '1')->get();
        $roles = Role::all();
        $matters = Matter::where('flag', 1)->get();

        return view('pages.admin.visit.create', compact('visitor', 'daerahPondok', 'roles', 'matters'));
    }

    public function edit($id)
    {
        $item = Visit::find($id);
        $visitor = Member::where('id_role', '4')->get();
        $daerahPondok = DaerahPondok::where('flag', '1')->get();
        $matters = Matter::where('flag', 1)->get();
        $roles = Role::all();

        return view('pages.admin.visit.edit', compact('visitor', 'matters', 'daerahPondok', 'roles', 'item'));
    }

    public function store(Request $request)
    {
        # code...
        return $this->service->store($request);
    }

    public function update(Request $request, $id)
    {
        # code...
        return $this->service->update($request, $id);
    }

    public function destroy($id)
    {
        # code...
        return $this->service->destroy($id);
    }
}
