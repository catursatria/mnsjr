<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\DaerahPondok;
use App\Models\Member;
use App\Models\Status;
use App\Services\Admin\MemberService;
use Illuminate\Http\Request;

class MemberController extends Controller
{
    protected $service;

    public function __construct(
        MemberService $service

    )
    {
        $this->service = $service;
    }

    public function index()
    {
        return view('pages.admin.member.index');
    }

    public function data(Request $request)
    {
        return $this->service->data($request);
    }

    public function edit($id)
    {
        $item = Member::find($id);
        $asalPondok = DaerahPondok::where([['jenis', 1],['flag', 1]])->orWhere([['jenis', 2],['flag', 1]])->orWhere([['jenis', 3],['flag', 1]])->orderBy('jenis', 'asc')->get();
        $tempatTugas = DaerahPondok::where('flag', 1)->orderBy('jenis', 'asc')->get();
        $roles = Status::all();

        return view('pages.admin.member.edit', compact('item', 'asalPondok','tempatTugas', 'roles'));
    }

    public function create()
    {
        $asalPondok = DaerahPondok::where([['jenis', 1],['flag', 1]])->orWhere([['jenis', 2],['flag', 1]])->orWhere([['jenis', 3],['flag', 1]])->orderBy('jenis', 'asc')->get();
        $tempatTugas = DaerahPondok::where('flag', 1)->orderBy('jenis', 'asc')->get();
        $roles = Status::all();

        return view('pages.admin.member.create', compact('asalPondok','tempatTugas','roles'));
    }

    public function store(Request $request)
    {
        $this->service->store($request);
        return redirect()->route('member.index')->with('success', 'Data has been created.');
    }

    public function update(Request $request, $id)
    {
        $this->service->update($request, $id);
        return redirect()->route('member.index')->with('success', 'Data has been updated.');
    }

    public function destroy($id)
    {
        return $this->service->destroy($id);
    }
}
