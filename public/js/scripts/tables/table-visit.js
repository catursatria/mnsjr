/**
 * DataTables Basic
 */

// const { get } = require("jquery");

$(function () {
    'use strict';

    var dt_basic_table = $('.datatables-basic'),
      dt_date_table = $('.dt-date'),
      assetPath = '../../../app-assets/';

    if ($('body').attr('data-framework') === 'laravel') {
      assetPath = $('body').attr('data-asset-path');
    }

    // DataTable with buttons
    // --------------------------------------------------------------------

    if (dt_basic_table.length) {
      var dt_basic = dt_basic_table.DataTable({
          processing :true,
          serverSide :true,
          ajax: {
              url: '/admin/datatables/visit/all',
              type: "GET",
              headers: {
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                  'Content-Type': 'application/json',
                  Accept: 'application/json'
                },
                contentType: 'application/json',
                dataType: 'json'
          },
        columns: [
          { data: 'dari' },
          { data: 'sampai' },
          { data: 'daerah.nama' },
          { data: 'visitor1.nama' },
          { data: 'visitor2.nama' },
          { data: 'visitor3.nama' },
          { data: 'action' },
        ],
        columnDefs: [
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 0
          },
          {
            // For Responsive
            width: '10%',
            //   responsivePriority: 2,
            targets: 1
          },
          {
            // For Responsive
            width: '15%',
            //   responsivePriority: 2,
            targets: 2
          },
          {
            // For Responsive
            width: '20%',
            //   responsivePriority: 2,
            targets: 3,
            render: function (data, type, full, meta) {
              if (full['id_visitor1'] == null) {
                return '';
              } else {
                  return data;
              }
            }
          },
          {
            // For Responsive
            width: '20%',
            //   responsivePriority: 2,
            targets: 4,
            render: function (data, type, full, meta) {
              if (full['id_visitor2'] == null) {
                return '';
              } else {
                  return data;
              }
            }
          },
          {
            // For Responsive
            width: '20%',
            targets: 5,
            render: function (data, type, full, meta) {
              if (full['id_visitor3'] == null) {
                return '';
              } else {
                  return data;
              }
            }
            //   responsivePriority: 2,
          },
          {
            // Actions
            targets: -1,
            title: 'Actions',
            orderable: false,
            width: '10%',
            render: function (data, type, full, meta) {
                var $id = full['id'];

                var $action = '<d class="d-inline-flex">';

                return (
                //   $action +
                  '<a href="javascript:;" class="item-edit btn-detail p-25" data-item="' + $id + '">' +
                  feather.icons['eye'].toSvg({ class: 'font-small-4' }) +
                  '</a>'+
                  '<a href="javascript:;" class="item-edit btn-delete p-25" data-item="' + $id + '">' +
                  feather.icons['trash'].toSvg({ class: 'font-small-4' }) +
                  '</a>'
                );

              }
          }
        ],
        dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        // dom: '<"card-header border-bottom p-1"<"head-label"><"dt-action-buttons text-end"B>><"d-flex justify-content-between align-items-center mx-0 row"<"col-sm-12 col-md-6"l><"col-sm-12 col-md-6"f>>t<"d-flex justify-content-between mx-0 row"<"col-sm-12 col-md-6"i><"col-sm-12 col-md-6"p>>',
        displayLength: 10,
        lengthMenu: [10, 25, 50],
        buttons: [
          {
            text: feather.icons['plus'].toSvg({ class: 'me-50 font-small-4' }) + 'Add New Record',
            className: 'create-new btn btn-primary',
            action: function ( e, dt, node, config ) {
                location.href = '/admin/visit/create';
            }
          }
        ],
        responsive: {
          details: {
            display: $.fn.dataTable.Responsive.display.modal({
              header: function (row) {
                var data = row.data();
                return 'Details of ' + data['full_name'];
              }
            }),
            type: 'column',
            renderer: function (api, rowIdx, columns) {
              var data = $.map(columns, function (col, i) {
                return col.title !== '' // ? Do not show row in modal popup if title is blank (for check box)
                  ? '<tr data-dt-row="' +
                      col.rowIdx +
                      '" data-dt-column="' +
                      col.columnIndex +
                      '">' +
                      '<td>' +
                      col.title +
                      ':' +
                      '</td> ' +
                      '<td>' +
                      col.data +
                      '</td>' +
                      '</tr>'
                  : '';
              }).join('');

              return data ? $('<table class="table"/>').append('<tbody>' + data + '</tbody>') : false;
            }
          }
        },
        language: {
          paginate: {
            // remove previous & next text from pagination
            previous: '&nbsp;',
            next: '&nbsp;'
          }
        }
      });
      $('div.head-label').html('<h6 class="mb-0">Visit</h6>');
    }

    $(document).on('click', '.btn-detail', function (e) {
        e.preventDefault();
        if ($(this).attr('data-item') !== 'undefined' && $(this).attr('data-item') !== ''){
            location.href = '/admin/visit/' + $(this).attr('data-item')+'/edit';
        }
      });

      var item_id;

      // Delete action
      $(document).on("click", ".btn-delete", function (e) {
          e.preventDefault();
          if (
              $(this).attr("data-item") !== "undefined" &&
              $(this).attr("data-item") !== ""
          ) {
              item_id = $(this).attr("data-item");
              $("#deleteModal").modal("show");
          }
      });

      $("#ok_button").click(function () {
          $.ajaxSetup({
              headers: {
                  "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
              },
              type: "DELETE",
              url: "/admin/visit/" + item_id,
          });
          $.ajax({
              beforeSend: function () {
                  $("#ok_button").text("Deleting...");
              },
              success: function (data) {
                  setTimeout(function () {
                      $("#deleteModal").modal("hide");
                      $("#ok_button").text("OK");
                      $(".datatables-basic").DataTable().ajax.reload();
                  }, 1000);
              },
          });
      });
  });
