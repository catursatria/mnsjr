<?php

namespace Database\Seeders;

use App\Models\DaerahPondok;
use Illuminate\Database\Seeder;

class DaerahPondokSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DaerahPondok::create([
            'nama' => 'Shirotol Mustaqim, Bali',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Insan Mulya, Yogyakarta 1',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Sumber Barokah, Karawang',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Darul Huffadz, Bogor Selatan 2',
            'jenis' => 5
        ]);
        DaerahPondok::create([
            'nama' => 'Baitul Manshurin, Mojokerto',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Gadingmangu',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Baitul Manshurin, Malang Timur',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Wali Barokah, Kediri',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Balikpapan',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Demak',
            'jenis' => 4
        ]);
        DaerahPondok::create([
            'nama' => 'Yogyakarta 3',
            'jenis' => 4
        ]);
        DaerahPondok::create([
            'nama' => 'Baitul Ulya, Karawang Timur',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Darul Ilmi, Bandung Timur 2',
            'jenis' => 5
        ]);
        DaerahPondok::create([
            'nama' => 'Nurul Huda, Lampung Selatan',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Nashrullah, Indramayu',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Budi Mulia, Tangerang Barat',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Baitul Mursyid, Bekasi Barat',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Roudhotul Ilmi, Cikarang',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Roudhotul Jannah, Makassar Utara',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Daarul Ilmi, Bogor Utara',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Al-Kautsar, Cirebon',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Solo Selatan',
            'jenis' => 4
        ]);
        DaerahPondok::create([
            'nama' => 'Baitul Huda, Subang',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Nurul Aini, Tangerang Timur',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'As-Syifa, Solo Selatan',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Nurhasan, Nganjuk Barat',
            'jenis' => 3
        ]);
        DaerahPondok::create([
            'nama' => 'Surabaya Utara',
            'jenis' => 4
        ]);
        DaerahPondok::create([
            'nama' => 'Nurhasan, Blitar',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Al-Fattah, Palembang',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Palu',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Palembang',
            'jenis' => 4
        ]);
        DaerahPondok::create([
            'nama' => 'Rasana Rasyida, Garut',
            'jenis' => 1
        ]);
        DaerahPondok::create([
            'nama' => 'Birul Walidain, Bojonegoro',
            'jenis' => 3
        ]);

    }
}
