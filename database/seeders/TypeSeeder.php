<?php

namespace Database\Seeders;

use App\Models\Type;
use Illuminate\Database\Seeder;

class TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Type::create([
            'nama'=>'Pondok Pesantren'
        ]);
        Type::create([
            'nama'=>'Pondok Pesantren Mahasiswa'
        ]);
        Type::create([
            'nama'=>'Pondok Pesantren Pelajar & Mahasiswa'
        ]);
        Type::create([
            'nama'=>'Daerah'
        ]);
        Type::create([
            'nama'=>'Pondok Schooling'
        ]);
    }
}
