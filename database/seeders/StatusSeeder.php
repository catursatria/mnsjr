<?php

namespace Database\Seeders;

use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        Status::create([
            'name'=>'M-NSJR'
        ]);
        Status::create([
            'name'=>'Alumni'
        ]);
        Status::create([
            'name'=>'Member'
        ]);
        Status::create([
            'name'=>'Intensive Class'
        ]);
    }
}
