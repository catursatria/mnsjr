<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProgramsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('programs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama')->nullable();
            $table->string('jenis')->nullable();
            $table->string('sejak')->nullable();
            $table->string('hingga')->nullable();
            $table->string('hingga_detail')->nullable();
            $table->string('keterangan')->nullable();
            $table->string('foto')->nullable();
            $table->tinyInteger('flag')->default(1);
            $table->integer('id_daerah_pondok')->nullable();
            $table->integer('created_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('programs');
    }
}
