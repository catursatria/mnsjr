<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDaerahPondokTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('daerah_pondok', function (Blueprint $table) {
            $table->id();
            $table->string('nama')->nullable();
            $table->integer('jenis')->nullable();
            $table->string('bawah_bimbingan')->nullable();
            $table->string('bawah_bimbingan_lainnya')->nullable();
            $table->string('ruang_lingkup')->nullable();
            $table->string('ruang_lingkup_lainnya')->nullable();
            $table->tinyInteger('flag')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('daerah_pondok');
    }
}
