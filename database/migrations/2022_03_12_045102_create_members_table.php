<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('members', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date('tanggal_lahir')->nullable();
            $table->string('no_hp')->nullable();
            $table->string('email')->unique();
            $table->integer('id_pondok_asal')->nullable();
            $table->integer('id_tempat_tugas')->nullable();
            $table->string('daerah_sambung')->nullable();
            $table->string('desa_sambung')->nullable();
            $table->string('kelompok_sambung')->nullable();
            $table->text('alamat')->nullable();
            $table->integer('status')->nullable();
            $table->integer('status_sekolah')->nullable();
            $table->integer('status_kelas3')->nullable();
            $table->tinyInteger('flag')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('members');
    }
}
