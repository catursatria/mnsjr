<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVisitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visits', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('id_visitor1')->nullable();
            $table->integer('id_visitor2')->nullable();
            $table->integer('id_visitor3')->nullable();
            $table->integer('id_materi1')->nullable();
            $table->integer('id_materi2')->nullable();
            $table->integer('id_materi3')->nullable();
            $table->integer('id_materi4')->nullable();
            $table->integer('id_materi5')->nullable();
            $table->integer('id_daerah_pondok')->nullable();
            $table->date('dari')->nullable();
            $table->date('sampai')->nullable();
            $table->text('materi')->nullable();
            $table->text('kendala')->nullable();
            $table->text('kebutuhan')->nullable();
            $table->text('saran')->nullable();
            $table->tinyInteger('flag')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visits');
    }
}
