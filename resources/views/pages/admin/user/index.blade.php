@extends('layouts/contentLayoutMaster')

@section('title', 'Data User')

@section('vendor-style')
    {{-- vendor css files --}}
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/dataTables.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/responsive.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/buttons.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/tables/datatable/rowGroup.bootstrap5.min.css')) }}">
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/pickers/flatpickr/flatpickr.min.css')) }}">
@endsection

@section('content')
    @if (session('success'))
        <div class="alert alert-success alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Success</h4>
            <div class="alert-body">
                {{ session('success') }}
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Warning</h4>
            <div class="alert-body">
                {{ session('failed') }}
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show mb-0" role="alert">
            <ul>
                @foreach ($errors->all() as $error)
                    <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close">
                    </button>
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <!-- Basic table -->
    <section id="basic-datatable">
        <div class="row">
            <div class="col-12">
                <div class="card">
                    <table class="datatables-basic table">
                        <thead>
                            <tr>
                                <th>NIK</th>
                                <th>Name</th>
                                <th>Phone</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
        <!-- Modal Delete -->
        <div id="deleteModal" class="modal fade" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h2 class="modal-title">Confirmation</h2>
                    </div>
                    <div class="modal-body">
                        <h4 align="center" style="margin:0;">Are you sure you want to remove this data?</h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" name="ok_button" id="ok_button" class="btn btn-danger">OK</button>
                        <button type="reset" class="btn btn-default" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- Modal to add new record -->
        <div class="modal modal-slide-in fade" id="modals-slide-in">
            <div class="modal-dialog sidebar-sm">
                <form class="add-new-record modal-content pt-0" action="{{ route('users.store') }}" method="post"
                    enctype="multipart/form-data">
                    @csrf
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close">×</button>
                    <div class="modal-header mb-1">
                        <h5 class="modal-title" id="exampleModalLabel">New Record</h5>
                    </div>
                    <div class="modal-body flex-grow-1">
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-nip">NIP (Nomor Induk Pegawai)</label>
                            <input type="text" class="form-control dt-full-name" id="basic-icon-default-nip" name="nip" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-nik">NIK (Nomor Induk
                                Kependudukan)</label>
                            <input type="text" class="form-control dt-full-name" id="basic-icon-default-nik" name="nik" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-fullname">Name</label>
                            <input type="text" class="form-control dt-full-name" id="basic-icon-default-fullname"
                                name="name" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-email">Email</label>
                            <input type="email" class="form-control dt-full-name" id="basic-icon-default-email"
                                name="email" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-password">Password</label>
                            <input type="password" class="form-control dt-full-name" id="basic-icon-default-password"
                                name="password" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-phone">Phone</label>
                            <input type="text" class="form-control dt-full-name" id="basic-icon-default-phone"
                                name="phone" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-address">Address</label>
                            <textarea name="address" class="form-control" id="basic-icon-default-address" cols="30"
                                rows="5"></textarea>
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-image">Image</label>
                            <input type="file" class="form-control dt-full-name" id="basic-icon-default-image"
                                name="image" />
                        </div>
                        <div class="mb-1">
                            <label class="form-label" for="basic-icon-default-upline">Upline</label>
                            <select name="upline_id" class="form-control" id="basic-icon-default-upline">
                                <option value="">- Pilih Upline -</option>
                                <option value="">Tidak Ada</option>
                                @foreach ($upline as $item)
                                    <option value="{{ $item->id }}"
                                        {{ old('upline_id') == $item->id ? 'selected' : '' }}>{{ $item->name }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="mb-3">
                            <label class="form-label" for="basic-icon-default-role">Role</label>
                            <select name="role" class="form-control" id="basic-icon-default-role">
                                <option value="">- Pilih Role -</option>
                                @foreach ($roles as $item)
                                    <option value="{{ $item->name }}"
                                        {{ old('role') == $item->name ? 'selected' : '' }}>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <button type="submit" class="btn btn-primary me-1">Submit</button>
                        <button type="reset" class="btn btn-outline-secondary" data-bs-dismiss="modal">Cancel</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--/ Basic table -->
@endsection


@section('vendor-script')
    {{-- vendor files --}}
    <script src="{{ asset(mix('vendors/js/tables/datatable/jquery.dataTables.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.responsive.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/responsive.bootstrap5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.checkboxes.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/datatables.buttons.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/jszip.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/pdfmake.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/vfs_fonts.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.html5.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/buttons.print.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/tables/datatable/dataTables.rowGroup.min.js')) }}"></script>
    <script src="{{ asset(mix('vendors/js/pickers/flatpickr/flatpickr.min.js')) }}"></script>
@endsection

@section('page-script')
    {{-- Page js files --}}
    <script src="{{ asset('js/scripts/tables/table-master-user.js') }}"></script>
@endsection
