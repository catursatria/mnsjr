@extends('layouts/contentLayoutMaster')

@section('title', 'Form Edit')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
    <!-- Basic Horizontal form layout section start -->

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Success</h4>
            <div class="alert-body">
                {{ session('success') }}
            </div>
        </div>
    @endif
    @if ($errors->any())
        <div class="alert alert-danger alert-dismissible fade show" role="alert">
            <h4 class="alert-heading">Warning</h4>
            <div class="alert-body">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
        </div>
    @endif
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Create Form</h4>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('member.update', $item->id) }}" method="post" enctype="multipart/form-data">
                            @method('PUT')
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="nama">Nama</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-default @error('nama') is-invalid @enderror" name="nama"
                                            value="{{ $item->nama }}" autocomplete="off" required>
                                            @error('nama')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Status</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="status" class="form-control" autocomplete="off" required>
                                                <option value="">Pilih Status</option>
                                                @foreach ($roles as $value)
                                                    <option value="{{ $value->id }}"
                                                        {{$item->status==$value->id ? 'selected' : ''}}>{{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                            @error('status')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Tempat Lahir</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-default @error('tempat_lahir') is-invalid @enderror" name="tempat_lahir"
                                            value="{{ $item->tempat_lahir }}" autocomplete="off" required>
                                            @error('tempat_lahir')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Tanggal Lahir</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="date" class="form-control input-default @error('tanggal_lahir') is-invalid @enderror" name="tanggal_lahir" autocomplete="off" required
                                            value="{{ $item->tanggal_lahir }}" id="ttl">
                                            @error('tanggal_lahir')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Nomor HP</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-default @error('no_hp') is-invalid @enderror" name="no_hp"
                                            value="{{ $item->no_hp }}" autocomplete="off" required>
                                            @error('no_hp')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Email</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="email" class="form-control input-default @error('email') is-invalid @enderror" name="email"
                                            value="{{ $item->email }}" autocomplete="off" required>
                                            @error('email')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Daerah Sambung</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-default @error('daerah_sambung') is-invalid @enderror" name="daerah_sambung"
                                            value="{{ $item->daerah_sambung }}" autocomplete="off" required>
                                            @error('daerah_sambung')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Desa Sambung</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-default @error('desa_sambung') is-invalid @enderror" name="desa_sambung"
                                            value="{{ $item->desa_sambung }}" autocomplete="off" required>
                                            @error('desa_sambung')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Kelompok Sambung</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <input type="text" class="form-control input-default @error('kelompok_sambung') is-invalid @enderror" name="kelompok_sambung"
                                            value="{{ $item->kelompok_sambung }}" autocomplete="off" required>
                                            @error('kelompok_sambung')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="col-sm-3">
                                            <label for="">Alamat</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <textarea class="form-control input-default @error('alamat') is-invalid @enderror" name="alamat" cols="30" rows="3" autocomplete="off" required>{{ $item->alamat }}</textarea>
                                            @error('alamat')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="col-sm-3">
                                            <label for="">Pondok Asal</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="id_pondok_asal" class="form-control" autocomplete="off" required>
                                                <option value="">Pilih Pondok Asal</option>
                                                @foreach ($asalPondok as $value)
                                                    <option value="{{ $value->id }}"
                                                        {{$item->id_pondok_asal==$value->id ? 'selected' : ''}}>
                                                        {{ $item->type->nama ?? ''}} {{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                            @error('id_pondok_asal')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="col-sm-3">
                                            <label for="">Tempat Tugas</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="id_tempat_tugas" class="form-control" autocomplete="off" required>
                                                <option value="">Pilih Tempat Tugas</option>
                                                @foreach ($asalPondok as $value)
                                                    <option value="{{ $value->id }}"
                                                        {{$item->id_tempat_tugas==$value->id ? 'selected' : ''}}>
                                                        {{ $item->type->nama ?? ''}} {{ $value->nama }}</option>
                                                @endforeach
                                            </select>
                                            @error('id_tempat_tugas')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3 row">
                                        <div class="col-sm-3">
                                            <label for="">Status Sekolah</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="status_sekolah" class="form-control" autocomplete="off" required>
                                                <option value="">Pilih Status Sekolah</option>
                                                <option value="1" {{$item->status_sekolah==1 ? 'selected' : ''}}>SMP</option>
                                                <option value="2" {{$item->status_sekolah==2 ? 'selected' : ''}}>SMA/SMK</option>
                                                <option value="3" {{$item->status_sekolah==3 ? 'selected' : ''}}>Mahasiswa</option>
                                                <option value="4" {{$item->status_sekolah==4 ? 'selected' : ''}}>Bekerja</option>
                                                <option value="5" {{$item->status_sekolah==5 ? 'selected' : ''}}>Lain-lain</option>
                                            </select>
                                            @error('status_sekolah')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                    <div class="mb-3 row kelas3">
                                        <div class="col-sm-3">
                                            <label for="">Kelas Sekolah</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="status_kelas3" class="form-control" autocomplete="off">
                                                <option value="">Pilih Status Kelas 3</option>
                                                <option value="1" {{$item->status_kelas3=="1" ? 'selected' : ''}}>Ya</option>
                                                <option value="0" {{$item->status_kelas3=="0" ? 'selected' : ''}}>Tidak</option>
                                            </select>
                                            @error('status_kelas3')
                                                <div class="invalid-feedback">{{ $message }}</div>
                                            @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="col-sm-9 offset-sm-3">
                                    <button type="submit" class="btn btn-primary me-1">Update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection

@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
    <!-- Page js files -->
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
    <script>
        window.onload = function() {
            $("select[name=status_sekolah] option:selected").each(function() {
                var value = $(this).val();
                if (value == "1" || value == "2") {
                    $('.kelas3').show();

                } else {
                    $('.kelas3').hide();
                }
            });
        }
        jQuery(document).ready(function($) {
            $('select[name=status_sekolah]').change(function() {
                $("select[name=status_sekolah] option:selected").each(function() {
                    var value = $(this).val();
                    if (value == "1" || value == "2") {
                        $('.kelas3').show();

                    } else {
                        $('.kelas3').hide();
                    }
                });
            });
        });
    </script>
@endsection
