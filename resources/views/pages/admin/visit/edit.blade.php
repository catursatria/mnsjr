@extends('layouts/contentLayoutMaster')

@section('title', 'Form Edit')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection
@section('content')
    <!-- Basic Horizontal form layout section start -->

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Success</h4>
            <div class="alert-body">
                {{ session('success') }}
            </div>
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Warning</h4>
            <div class="alert-body">
                {{ session('failed') }}
            </div>
        </div>
    @endif
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Edit Form</h4>
                    </div>
                    <div class="card-body">
                        <form class="form form-vertical" action="{{ route('visit.update', $item->id) }}" method="POST">
                            @csrf
                            @method('put')
                          <div class="row">
                            <div class="col-12">
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label for="">Tujuan Visit</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select name="id_daerah_pondok" id="id_daerah_pondok" class="form-control">
                                            <option value="">Pilih Daerah / Pondok</option>
                                            @foreach ($daerahPondok as $value)
                                                <option value="{{ $value->id }}"
                                                    {{$item->id_daerah_pondok==$value->id ? 'selected' : ''}}>
                                                    @if ($value->jenis == 1)
                                                Pondok Pesantren
                                                @elseif ($value->jenis == 2)
                                                Pondok Pesantren Mahasiswa
                                                @elseif ($value->jenis == 3)
                                                Pondok Pesantren Pelajar & Mahasiswa
                                                @elseif ($value->jenis == 4)
                                                Daerah
                                                @endif
                                                {{ $value->nama }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="select2-multiple">Visitor</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="select2 form-select" multiple="true multiple" name="id_visitor1[]"
                                            id="select2-multiple" multiple>
                                            <optgroup label="Visitor">
                                                @foreach ($visitor as $value)
                                                    <option value="{{ $value->id }}"
                                                        @if ($value->id == $item->id_visitor1)
                                                        selected
                                                        @elseif ($value->id == $item->id_visitor2)
                                                        selected
                                                        @elseif ($value->id == $item->id_visitor3)
                                                        selected
                                                    @endif>{{ $value->nama }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                                <div class="mb-1 row">
                                    <div class="col-sm-3">
                                        <label class="col-form-label" for="select2-multiple">Materi</label>
                                    </div>
                                    <div class="col-sm-9">
                                        <select class="select2 form-select" multiple="true multiple" name="id_materi1[]"
                                            id="select2-multiple2" multiple>
                                            <optgroup label="Materi">
                                                @foreach ($matters as $value)
                                                    <option value="{{ $value->id }}"
                                                        @if ($value->id == $item->id_materi1)
                                                        selected
                                                        @elseif ($value->id == $item->id_materi2)
                                                        selected
                                                        @elseif ($value->id == $item->id_materi3)
                                                        selected
                                                        @elseif ($value->id == $item->id_materi4)
                                                        selected
                                                        @elseif ($value->id == $item->id_materi5)
                                                        selected
                                                        @endif
                                                        >{{ $value->nama }}</option>
                                                @endforeach
                                            </optgroup>
                                        </select>
                                    </div>
                                </div>
                              <div class="mb-3">
                                  <div class="row">
                                      <div class="col-6">
                                          <label class="form-label" for="dari">Dari</label>
                                          <input type="date"
                                              class="form-control input-default @error('dari') is-invalid @enderror"
                                              name="dari" autocomplete="off" required value="{{ $item->dari }}"
                                              id="dari">
                                          @error('dari')
                                              <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                      </div>
                                      <div class="col-6">
                                          <label class="form-label" for="sampai">Sampai</label>
                                          <input type="date"
                                              class="form-control input-default @error('sampai') is-invalid @enderror"
                                              name="sampai" autocomplete="off" required value="{{ $item->sampai }}"
                                              id="sampai">
                                          @error('sampai')
                                              <div class="invalid-feedback">{{ $message }}</div>
                                          @enderror
                                      </div>
                                  </div>
                              </div>
                              <div class="mb-3">
                                <label class="form-label" for="kendala">Kendala</label>
                                <textarea name="kendala" class="form-control ckeditor" id="kendala" cols="30" rows="10">{{ $item->kendala }}</textarea>
                              </div>
                              <div class="mb-3">
                                <label class="form-label" for="kebutuhan">Kebutuhan</label>
                                <textarea name="kebutuhan" class="form-control ckeditor" id="kebutuhan" cols="30" rows="10">{{ $item->kebutuhan }}</textarea>
                              </div>
                              <div class="mb-3">
                                <label class="form-label" for="saran">Saran</label>
                                <textarea name="saran" class="form-control ckeditor" id="saran" cols="30" rows="10">{{ $item->saran }}</textarea>
                              </div>
                            </div>
                            <div class="col-12">
                              <button type="submit" class="btn btn-primary me-1 waves-effect waves-float waves-light">Update</button>
                            </div>
                          </div>
                        </form>
                      </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="//cdn.ckeditor.com/4.17.2/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
@endsection
