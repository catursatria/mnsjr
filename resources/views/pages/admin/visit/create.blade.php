@extends('layouts/contentLayoutMaster')

@section('title', 'Form Create')

@section('vendor-style')
    <!-- vendor css files -->
    <link rel="stylesheet" href="{{ asset(mix('vendors/css/forms/select/select2.min.css')) }}">
@endsection

@section('content')
    <!-- Basic Horizontal form layout section start -->

    @if (session('success'))
        <div class="alert alert-success" role="alert">
            <h4 class="alert-heading">Success</h4>
            <div class="alert-body">
                {{ session('success') }}
            </div>
        </div>
    @endif
    @if (session('failed'))
        <div class="alert alert-danger" role="alert">
            <h4 class="alert-heading">Warning</h4>
            <div class="alert-body">
                {{ session('failed') }}
            </div>
        </div>
    @endif
    <section id="basic-horizontal-layouts">
        <div class="row">
            <div class="col-md-12 col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Create Form</h4>
                    </div>
                    <div class="card-body">
                        <form class="form form-vertical" action="{{ route('visit.store') }}" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col-12">
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label for="">Tujuan Visit</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select name="id_daerah_pondok" id="id_daerah_pondok" class="form-control">
                                                <option value="">Pilih Daerah / Pondok</option>
                                                @foreach ($daerahPondok as $value)
                                                    <option value="{{ $value->id }}"
                                                        {{ old('id_daerah_pondok') == $value->id ? 'selected' : '' }}>
                                                        {{ $value->type->nama }} {{ $value->nama }}
                                                    </option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="select2-multiple">Visitor</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="select2 form-select" multiple="true multiple" name="id_visitor1[]"
                                                id="select2-multiple" multiple>
                                                <optgroup label="Visitor">
                                                    @foreach ($visitor as $value)
                                                        <option value="{{ $value->id }}">{{ $value->nama }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-1 row">
                                        <div class="col-sm-3">
                                            <label class="col-form-label" for="select2-multiple">Materi</label>
                                        </div>
                                        <div class="col-sm-9">
                                            <select class="select2 form-select" multiple="true multiple" name="id_materi1[]"
                                                id="select2-multiple2" multiple>
                                                <optgroup label="Materi">
                                                    @foreach ($matters as $value)
                                                        <option value="{{ $value->id }}">{{ $value->nama }}</option>
                                                    @endforeach
                                                </optgroup>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="mb-3">
                                        <div class="row">
                                            <div class="col-6">
                                                <label class="form-label" for="dari">Dari</label>
                                                <input type="date"
                                                    class="form-control input-default @error('dari') is-invalid @enderror"
                                                    name="dari" autocomplete="off" required value="{{ old('dari') }}"
                                                    id="dari">
                                                @error('dari')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                            <div class="col-6">
                                                <label class="form-label" for="sampai">Sampai</label>
                                                <input type="date"
                                                    class="form-control input-default @error('sampai') is-invalid @enderror"
                                                    name="sampai" autocomplete="off" required value="{{ old('sampai') }}"
                                                    id="sampai">
                                                @error('sampai')
                                                    <div class="invalid-feedback">{{ $message }}</div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    {{-- <div class="mb-3">
                                        <label class="form-label" for="materi">Materi</label>
                                        <textarea name="materi" class="form-control ckeditor" id="materi" cols="30"
                                            rows="10">{{ old('materi') }}</textarea>
                                    </div> --}}
                                    <div class="mb-3">
                                        <label class="form-label" for="kendala">Kendala</label>
                                        <textarea name="kendala" class="form-control ckeditor" id="kendala" cols="30"
                                            rows="10">{{ old('kendala') }}</textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label" for="kebutuhan">Kebutuhan</label>
                                        <textarea name="kebutuhan" class="form-control ckeditor" id="kebutuhan" cols="30"
                                            rows="10">{{ old('kebutuhan') }}</textarea>
                                    </div>
                                    <div class="mb-3">
                                        <label class="form-label" for="saran">Saran</label>
                                        <textarea name="saran" class="form-control ckeditor" id="saran" cols="30"
                                            rows="10">{{ old('saran') }}</textarea>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <button type="submit"
                                        class="btn btn-primary me-1 waves-effect waves-float waves-light">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Basic Horizontal form layout section end -->

@endsection
@section('vendor-script')
    <!-- vendor files -->
    <script src="{{ asset(mix('vendors/js/forms/select/select2.full.min.js')) }}"></script>
@endsection
@section('page-script')
    <script src="//cdn.ckeditor.com/4.17.2/standard/ckeditor.js"></script>
    <script>
        $(document).ready(function() {
            $('.ckeditor').ckeditor();
        });
    </script>
    <script src="{{ asset(mix('js/scripts/forms/form-select2.js')) }}"></script>
@endsection
