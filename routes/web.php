<?php

use App\Http\Controllers\Admin\DaerahPondokController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\MemberController;
use App\Http\Controllers\Admin\ProgramController;
use App\Http\Controllers\Admin\UserController;
use App\Http\Controllers\Admin\VisitController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\StaterkitController;
use App\Http\Controllers\LanguageController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', [StaterkitController::class, 'home'])->name('home');
// Route::get('home', [StaterkitController::class, 'home'])->name('home');
// Route Components
Route::get('layouts/collapsed-menu', [StaterkitController::class, 'collapsed_menu'])->name('collapsed-menu');
Route::get('layouts/full', [StaterkitController::class, 'layout_full'])->name('layout-full');
Route::get('layouts/without-menu', [StaterkitController::class, 'without_menu'])->name('without-menu');
Route::get('layouts/empty', [StaterkitController::class, 'layout_empty'])->name('layout-empty');
Route::get('layouts/blank', [StaterkitController::class, 'layout_blank'])->name('layout-blank');
Route::get('/', [DashboardController::class, 'index']);
Route::prefix('admin')->group(function () {

    Route::post('member/{id}/delete', [MemberController::class, 'delete'])->name('member.delete');
    Route::resource('member', MemberController::class);
    Route::resource('user', UserController::class);
    Route::resource('daerah-pondok', DaerahPondokController::class);
    Route::resource('visit', VisitController::class);
    Route::resource('program', ProgramController::class);

    Route::group(['prefix' => 'datatables'], function () {
        Route::get('/permission/all', [PermissionsController::class, 'data'])->name('permission.data');
        Route::get('/role/all', [RolesController::class, 'data'])->name('role.data');
        Route::get('/users/all', [UserController::class, 'data'])->name('user.data');
        Route::get('/member/all', [MemberController::class, 'data'])->name('member.data');
        Route::get('/visit/all', [VisitController::class, 'data'])->name('visit.data');

    });
});


// locale Route
Route::get('lang/{locale}', [LanguageController::class, 'swap']);
